# Что-то полезное

**_Данная программа содержит в себе следующие возможности:_**

* Просмотр товара на складе
* Увеличение количества товара на складе
* Списание товара со склада
* Просмотр стоимости оставшихся товаров на складе

___

**_Для пользование данной программой необходимо:_**

* [ ] Скачать основную программу
* [ ] Скачать файлы с классами
* [ ] Скачать pickle файл с информацией о товарах

___

**_Пример кода, рассчитывающий стоимость оставшихся товаров на складе_**

```
def value_of_the_remaining_goods():
    print('.....Склады.....', end='\n\n')
    print('[1] Техника')
    print('[2] Мебель')
    print('[3] Одежда')
    print()
    print('Для выхода в главное меню введите "back"', end='\n\n')
    choice = input('Выберите склад: ')
    if choice == 'back':
        print('<<< Назад', end='\n\n')
        pass
    elif choice in '123':
        arr = []
        for j in store[int(choice) - 1].goods:
            arr.append(int(j.count*j.cost))
        print(f'Стоимость оставшихся товаров на складе - {j.types}: {sum(arr)}руб.',end='\n\n')
    else:
        print('Склада не существует',end='\n\n')
```
___

Рассчитывается стоимость по формуле:

$`j.count * j.cost`$

# Бесполезное

![alt Гриб](https://sun9-north.userapi.com/sun9-84/s/v1/ig2/dnB6XKknPKxuH9n0D7SjX5-MBFIMXGBEPWsJ1NW1fUDaE5JdqdpGvEyCWWLSxzgxxLrOZyAVVUcZjsczNZJjrlY-.jpg?size=996x996&quality=96&type=album)

___

[Легендарочка](https://www.youtube.com/watch?v=dQw4w9WgXcQ&ab_channel=RickAstley)
