def main():
    """
    Основное меню выбора действий

    Функция представляет из себя пользовательское меню,
    вызывающее различные функции кода с помощью оператора input
    :args: None
    :return: Пользовательское меню
    :raises: Ключи, несуществующие в словаре 'menu'
    """
    while True:
        print('.....Главное меню.....')
        print('[1] Просмотр товара на складе')
        print('[2] Увеличить количество товара на складе'),
        print('[3] Списать товар со склада')
        print('[4] Стоимость оставшихся товаров на складе')
        print()
        menu = {
            '1': show_goods,
            '2': add,
            '3': remove,
            '4': value_of_the_remaining_goods
        }
        print('Для закрытия программы введите "exit"')
        choice = input('>> Выберите пункт ')
        print()
        if choice == 'exit':
            print('.....Завершение работы.....')
            break
        if choice not in menu:
            print('Введён несуществующий пункт')
            print()
            continue
        foo = menu[choice]
        foo()


def load_file():
    """
    Функция считывает Pickle файл для дальнейшей работы
    :args: None
    :return: Pickle файл для дальнейшей работы
    :examples:
    Если файл имеет название - 'stores.pickle':
    >>> load_file()
    pickle.load(f)
    Если файл имеет любое другое название
    >>> load_file()
    'Склад не найден'
    """
    try:
        with open('stores.pickle', 'rb') as f:
            return pickle.load(f)
    except FileNotFoundError:
        print('Склад не найден')
        exit()

store = load_file()

def save_file():
    """
    Функция сохраняет в Pickle файл все изменения, сделанные в ходе работы программы
    :args: None
    :return: None
    """
    with open('stores.pickle', 'wb') as f:
            pickle.dump(store, f)
    load_file()


def add():
    """
    Функция добавляет на склад n-ое колличество единиц выбранного пользователем товара
    :args: None
    :return: Отчет о добавлении товара на склад
    :raises: Некорректно введёные данные
    """
    print('.....Склады.....',end='\n\n')
    print('[1] Техника')
    print('[2] Мебель')
    print('[3] Одежда')
    print()
    print('Для выхода в главное меню введите "back"', end='\n\n')
    choice = input('Выберите склад: ')
    if choice == 'back':
        print('<<< Назад', end='\n\n')
        pass
    elif choice in '123':
        while True:
            print('.....Список товаров.....',end='\n\n')
            for i in enumerate(store[int(choice)-1].goods):
                print(f'[{i[0]+1}]{i[1].name}')
                print()
            s_choice = input('Выберите товар: ')
            if s_choice == 'back':
                print('<<< Назад', end='\n\n')
                break
            elif s_choice in '123':
                t_choice = input('Добавить на склад(кол-во): ')
                if t_choice == 'back':
                    print('<<< Назад',end='\n\n')
                    break
                if not t_choice.isdigit():
                    print('Ошибка, введите число ', end='\n\n')
                    continue
                store[int(choice) - 1].goods[int(s_choice)-1].increase(int(t_choice))
                save_file()
                print(f'На склад добавлено {t_choice}шт. товара - {store[int(choice) - 1].goods[int(s_choice)-1].name}',end='\n\n')
                break
            else:
                print('Ошибка, товар не найден',end='\n\n')
                break
    else:
        print('Склад не найден', end='\n\n')

def remove():
    """
    Функция списывает со склада n-ое колличество единиц выбранного пользователем товара
    :args: None
    :return: Отчет о списании довара со склада
    :raises: Некорректно введёные данные
    """
    print('.....Склады.....',end='\n\n')
    print('[1] Техника')
    print('[2] Мебель')
    print('[3] Одежда')
    print()
    print('Для выхода в главное меню введите "back"', end='\n\n')
    choice = input('Выберите склад: ')
    if choice == 'back':
        print('<<< Назад', end='\n\n')
        pass
    elif choice in '123':
        while True:
            print('.....Список товаров.....',end='\n\n')
            for i in enumerate(store[int(choice)-1].goods):
                print(f'[{i[0]+1}]{i[1].name}')
                print()
            s_choice = input('Выберите товар: ')
            if s_choice == 'back':
                print('<<< Назад', end='\n\n')
                break
            elif s_choice in '123':
                t_choice = input('Списать со склада (кол-во): ')
                if t_choice == 'back':
                    print('<<< Назад', end='\n\n')
                    break
                if not t_choice.isdigit():
                    print('Ошибка, введите число ', end='\n\n')
                    continue
                store[int(choice) - 1].goods[int(s_choice)-1].decrease(int(t_choice))
                save_file()
                print(f'Со склада списано {t_choice}шт. товара - {store[int(choice) - 1].goods[int(s_choice)-1].name}',end='\n\n')
                break
            else:
                print('Ошибка, товар не найден',end='\n\n')
                break
    else:
        print('Склад не найден', end='\n\n')



def show_goods():
    """
    Функция, позволяющая просмотреть товары с определенного склада по схеме:
    "Наименование товара" - "Оставшееся количество на складе"
    :args: None
    :return: Печатает состояние товара с выбранного пользователем склада
    :raises: Некорректно введёные данные
    """
    print('.....Склады.....',end='\n\n')
    print('[1] Техника')
    print('[2] Мебель')
    print('[3] Одежда')
    print()
    print('Для выхода в главное меню введите "back"', end='\n\n')
    choice = input('Выберите склад: ')
    if choice == 'back':
        print('<<< Назад', end='\n\n')
        pass
    elif choice in '123':
        for j in store[int(choice)-1].goods:
            print(f'{j.name} - количество на складе: {j.count}',end='\n\n')
    else:
        print('Склада не существует',end='\n\n')

def value_of_the_remaining_goods():
    """
    Функция, рассчитывающая стоимость оставшихся на складе товаров
    :args: None
    :return: Печатает стоимость оставшихся товаров на выбранном пользователем складе
    :raises: Некорректно введёные данные
    """
    print('.....Склады.....', end='\n\n')
    print('[1] Техника')
    print('[2] Мебель')
    print('[3] Одежда')
    print()
    print('Для выхода в главное меню введите "back"', end='\n\n')
    choice = input('Выберите склад: ')
    if choice == 'back':
        print('<<< Назад', end='\n\n')
        pass
    elif choice in '123':
        arr = []
        for j in store[int(choice) - 1].goods:
            arr.append(int(j.count*j.cost))
        print(f'Стоимость оставшихся товаров на складе - {j.types}: {sum(arr)}руб.',end='\n\n')
    else:
        print('Склада не существует',end='\n\n')



if __name__ == '__main__':
    main()
